# VIRTBURG 3D game engine

I started my raycasting pseudo 3D software engine codenamed 3DMap in spring 1997
using 16-bit Borland C++ for DOS (VGA mode 320x200 on Pentium-60) and then
switched to 32-bit Watcom-C/C++ for DOS (still in 320x200):

![](https://gitlab.com/shaos/virtburg/raw/master/screenshots/OldPreVirtburg.jpg "old screenshot from 1997")

In 2000 I ported it to DirectDraw for Windows (SVGA mode 640x480 on Celeron
class machines and up) to use as a client for a Russian online multiplayer
game called Virtburg that was actually just a chat with 3D first-person view
where you can walk through a virtual city, see other visitors as sprites and
send/receive messages:

![](https://gitlab.com/shaos/virtburg/raw/master/screenshots/OldVirtburg.jpg "old screenshot from 2002")

Over 1200 people registered to participate since 2000 until I shut down
the server a decade later because of lack of activity and that's why I decided
to call this rendering engine "Virtburg 3D game engine" to open source.

In 2002 I ported the engine to Linux using SVGAlib to run on 486DX2-80 machine
with 12 MB RAM. Seven years later (2009) I finally ported it to SDL to run on
modern x86 and PowerPC machines with Linux in better resolution 800x600 and up.
Mouse support was added in 2011 and wide screen support was added just now (2020).

I decided to opensource this engine under liberal MIT license by combining
all source code to a single C-file with rewriting a few C++ parts to pure C for
consistency (it was a badly outdated C++ style there). Currently engine can
render like this in 1280x720 on AMD64 (video captured in real time):

[![](http://img.youtube.com/vi/pT2tuY47DDQ/2.jpg)](http://www.youtube.com/watch?v=pT2tuY47DDQ "Old Virtburg in HD (720p)")

STILL WORKING ON POLISHING SOURCES BEFORE RELEASING - STAY TUNED...

November 2020, Shaos <me@shaos.net>
